package com.mine.olimpiada.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zinsly, Tatiane
 * @email tzinsly@br.ibm.com
 */

@Controller
public class CadastroController {
	
	@RequestMapping("/cadastro-competicao")
	public String cadastro(){
		return "cadastro";
	}
	
}
