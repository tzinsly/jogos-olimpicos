package com.mine.olimpiada.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 
/**
 * @author Zinsly, Tatiane
 * @email tzinsly@br.ibm.com
 */


@SpringBootApplication
public class Configuracao {
 
	public static void main(String[] args) throws Exception {
        SpringApplication.run(Configuracao.class, args);
    }
	
}